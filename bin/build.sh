#!/bin/bash

./node_modules/.bin/mjml --config.validationLevel=skip src/EN/invoice-copy.mjml -o build/invoice-copy.html
./node_modules/.bin/mjml --config.validationLevel=skip src/EN/password-change.mjml -o build/password-change.html
./node_modules/.bin/mjml --config.validationLevel=skip src/EN/password-reset.mjml -o build/password-reset.html
./node_modules/.bin/mjml --config.validationLevel=skip src/EN/reporting-reminder.mjml -o build/reporting-reminder.html
./node_modules/.bin/mjml --config.validationLevel=skip src/EN/steward-registration-approved.mjml -o build/steward-registration-approved.html
./node_modules/.bin/mjml --config.validationLevel=skip src/FR/invoice-copy-fr.mjml -o build/invoice-copy-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/FR/password-change-fr.mjml -o build/password-change-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/FR/password-reset-fr.mjml -o build/password-reset-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/FR/reporting-reminder-fr.mjml -o build/reporting-reminder-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/FR/steward-registration-approved-fr.mjml -o build/steward-registration-approved-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/EN/invoice-copy.mjml -o build/yukon-invoice-copy.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/EN/password-change.mjml -o build/yukon-password-change.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/EN/password-reset.mjml -o build/yukon-password-reset.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/EN/reporting-reminder.mjml -o build/yukon-reporting-reminder.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/EN/steward-registration-approved.mjml -o build/yukon-steward-registration-approved.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/FR/invoice-copy-fr.mjml -o build/yukon-invoice-copy-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/FR/password-change-fr.mjml -o build/yukon-password-change-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/FR/password-reset-fr.mjml -o build/yukon-password-reset-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/FR/reporting-reminder-fr.mjml -o build/yukon-reporting-reminder-fr.html
./node_modules/.bin/mjml --config.validationLevel=skip src/Yukon/FR/steward-registration-approved-fr.mjml -o build/yukon-steward-registration-approved-fr.html

# Add any extra emails below
